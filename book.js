angular.module('bookSearch', [])

.controller('bookController', function($scope, $http) {

    $scope.logedin = false;

    $scope.currentUser = "";

    $scope.userInfo = {
        'email': '',
        'password': '',
        'searchResults': [],
        'likelist': []
    }

    // $scope.token = "27432a21";

    $scope.token = "";
    $scope.query = "";

    $scope.register = function() {
        var req = {
            method: 'POST',
            url: 'register',
            data: {
                'email': $scope.userInfo.email,
                'password': $scope.userInfo.password
            }

        }

        $http(req).success(function(data) {


            alert(data.message);

            console.log(data);

        }).error(function(error) {
            console.log('Error: ' + error);
        });
    }

    $scope.login = function() {
        var req = {
            method: 'POST',
            url: 'login',

            data: {
                'email': $scope.userInfo.email,
                'password': $scope.userInfo.password
            }

        }

        $http(req).success(function(data) {


            if (data.success) {

                $scope.token = data.token;

                $scope.likelist = [];

                $scope.list();

                $scope.logedin = true;

                $scope.currentUser = $scope.userInfo.email;
            }

            alert(data.message);

            console.log(data);

        }).error(function(error) {
            console.log('Error: ' + error);
        });
    }


    $scope.recomputeInList = function() {
        for (var i = 0; i < $scope.userInfo.searchResults.length; i++) {
            var book = $scope.userInfo.searchResults[i];

            if ($scope.isInLiked(book.id)) {
                book.inList = true;
            } else {
                book.inList = false;
            }
        };
    }

    $scope.search = function() {
        var req = {
            method: 'POST',
            url: 'search',

            data: {
                'query': $scope.query
            }

        }

        console.log($scope.query);

        $http(req).success(function(data) {

            if (data.success) {
                $scope.userInfo.searchResults = data.data;

                $scope.recomputeInList();


            } else {

                alert(data.message);
            }
            console.log(data);

        }).error(function(error) {
            console.log('Error: ' + error);
        });
    }


    $scope.like = function(id) {
        var req = {
            method: 'POST',
            url: 'like',

            data: {
                'token': $scope.token,
                'bookid': id
            }

        }

        console.log($scope.query);

        $http(req).success(function(data) {

            if (data.success) {
                $scope.userInfo.likelist = data.data;

                $scope.recomputeInList();

            } else {

                alert(data.message);
            }
            console.log(data);

        }).error(function(error) {
            console.log('Error: ' + error);
        });
    }


    $scope.list = function() {
        var req = {
            method: 'POST',
            url: 'list',

            data: {
                'token': $scope.token
            }

        }

        console.log($scope.query);

        $http(req).success(function(data) {

            if (data.success) {
                $scope.userInfo.likelist = data.data;

            } else {

                alert(data.message);
            }
            console.log(data);

        }).error(function(error) {
            console.log('Error: ' + error);
        });
    }


    $scope.remove = function(id) {
        var req = {
            method: 'POST',
            url: 'remove',

            data: {
                'token': $scope.token,
                'bookid': id
            }

        }

        console.log($scope.query);

        $http(req).success(function(data) {

            if (data.success) {
                $scope.userInfo.likelist = data.data;

                $scope.recomputeInList();

            } else {

                alert(data.message);
            }
            console.log(data);

        }).error(function(error) {
            console.log('Error: ' + error);
        });
    }


    $scope.isInLiked = function(id) {

        console.log("called");
        if ($scope.userInfo.likelist == undefined) {
            console.log()
            return false;
        }
        for (var i = 0; i < $scope.userInfo.likelist.length; i++) {
            if ($scope.userInfo.likelist[i].id == id) {

                console.log("liked");
                return true;
            }
        };


        return false;

    }


});